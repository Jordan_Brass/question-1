﻿using System;

namespace question_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string sAnswer;
            do 
            {            
            Console.WriteLine("What number would you like to see (Type exit to quit)");
            sAnswer = Console.ReadLine();
            Console.Clear();
            int answer;
            if (int.TryParse(sAnswer, out answer))
            {
                Console.WriteLine($"{answer} X 1 = {answer*1}");
                Console.WriteLine($"{answer} X 2 = {answer*2}");
                Console.WriteLine($"{answer} X 3 = {answer*3}");
                Console.WriteLine($"{answer} X 4 = {answer*4}");
                Console.WriteLine($"{answer} X 5 = {answer*5}");
                Console.WriteLine($"{answer} X 6 = {answer*6}");
                Console.WriteLine($"{answer} X 7 = {answer*7}");
                Console.WriteLine($"{answer} X 8 = {answer*8}");
                Console.WriteLine($"{answer} X 9 = {answer*9}");
                Console.WriteLine($"{answer} X 10 = {answer*10}");
                Console.WriteLine($"{answer} X 11 = {answer*11}");
                Console.WriteLine($"{answer} X 12 = {answer*12}");
            } 
            else 
            {
                Console.WriteLine("Please select a valid option");
            }
            } while(sAnswer != "exit");

            
            
        }
    }
}
